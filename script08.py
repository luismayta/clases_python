# Funciones


def f2(a, b):
    print a + b


def f3(a, b=10, c=20):
    print a + b + c

def main():
    # f2(1, 2)
    f3(4)
    f3(4, 5)
    f3(4, b=5)
    f3(4, c=4, b=20)

    kwargs = {'c': 4, 'b': 20}
    f3(4, **kwargs)


def f4(a, b):
    print a * b


def f5(a, b, c=4, d=5):
    print a + b + c + d

def main2():
    f4(1, 2)

    args = [1, 2]
    f4(*args)

    args = [3, 4]
    kwargs = {'d': 10, 'c': 20}
    f5(*args, **kwargs)


def f6(*args, **kwargs):
    print args, kwargs
    x = kwargs.get('apply_changes', False)
    if x:
        print 'Aplicar cambios'

def main3():
    f6(1, 2, 3, 4, c=4, f=5, g=8)

    args = (20, 40)
    kwargs = {'f': 58, 'x': 100}
    f6(*args, **kwargs)

    f6(apply_changes=True)

def main4():
    pass


def f7(l=None):
    l = l or []
    print l


def main5():
    f7()

if __name__ == '__main__':
    r = main5()
    print r
