# -*- coding: utf-8 -*-


class Persona(object):

    class Sexo(object):

        MASCULINO = 1
        FEMENINO = 0

    atributo = 'AATTRRR'

    def __init__(self, nombre, apellido, sexo):
        self.nombre = nombre
        self.apellido = apellido
        if sexo not in (self.Sexo.MASCULINO, self.Sexo.FEMENINO):
            raise ValueError('Sexo no válido')
        self.sexo = sexo

    @classmethod
    def metodo(clase):
        print clase, 'Método'

    def m(self):
        Persona.metodo()

    @staticmethod
    def estatico():
        print 'Estatico'

def main():
    p = Persona('jorge', 'puente', Persona.Sexo.MASCULINO)
    p.estatico()

if __name__ == '__main__':
    main()
