# -*- coding: utf-8 -*-
import sys


class MalTipo(TypeError):
    pass


def main():
    try:
        #a = 0
        #x = a / a
        raise MalTipo()
        a = 0
    except (ValueError, ZeroDivisionError):
        print 'ValueError'
    except TypeError:
        print 'TypeError'
    except MalTipo:
        print 'MalTipo'
    except RuntimeError:
        pass
    else: # Siempre es ejecutado, cuando no hay error.
        print 'else'
    finally: # Siempre es ejecutado, haya o no haya sido atrapada una excepción.
        print 'finally!'

if __name__ == '__main__':
    main()
