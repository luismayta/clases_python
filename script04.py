# -*- coding: utf-8 -*-
# Operadores lógicos


def main():
    a = 1
    b = 10

    print a == b
    print a != b
    print a < b
    print a <= b
    print a > b
    print a >= b

    b = 1
    print a is b
    print not a is b
    print a is not b

    x = [1, 2, 3]
    print a in x
    print not a in x

    print a is None
    print a is not None

if __name__ == '__main__':
    main()
