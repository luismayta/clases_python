# -*- coding: utf-8 -*-
# Manipulación de listas


def main():
    l = [10, 12, 14]
    print l, len(l)

    l.append(3)
    print l

    l.remove(10)
    print l

    l.pop()
    print l

    l[0] = 1000
    print l

    print l.index(14)

    l.insert(1, 999)
    print l

    print l * 4

    lista = l + [1, 2, 3]
    print lista, l

    l.extend([1, 2, 3])
    print l


if __name__ == '__main__':
    main()
