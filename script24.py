from collections import OrderedDict


def main():
    """
    l = [(i, i*2) for i in xrange(10) if i%2 == 0]
    print l

    d2 = OrderedDict(l)
    print d2

    d = {i: i*2 for i in xrange(10) if i%2 != 0}
    print d
    """

    l = range(20)
    k = [i*2 for i in l for i i]

if __name__ == '__main__':
    main()
