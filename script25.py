import six


def main():
    string = u'Esto es una cadena'
    print('type: %s' % type(string))
    if isinstance(string, six.string_types):
        print('Es una cadena!')
    else:
        print('No es una cadena!')


if __name__ == '__main__':
    main()
