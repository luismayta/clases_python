# -*- coding: utf-8 -*-


class A(object):  # A -> object
    pass


class B(object):  # B -> object
    pass


class C(A, B):  # C -> A -> B -> object
    pass


class D(C):  # D -> C -> A -> B -> object
    pass


# class E(A, D):
#     pass


def main():
    pass

if __name__ == '__main__':
    main()
