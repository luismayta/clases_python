# -*- coding: utf-8 -*-
# Manipulación de cadenas


def main():
    cad = 'Esta de una cadena'
    print cad, len(cad)

    print cad[5]
    print cad[5: 10]

    print 'cadena' in cad

    print '0' not in cad

    print cad.startswith('Es')
    print cad.endswith('ena')

    url = 'http://google.com'

    print url.startswith(('http://', 'https://'))

    print url.index('google')

    print url + cad
    print ''.join([url, cad])
    print '%s%s' % (url, cad)

    print url * 3

if __name__ == '__main__':
    main()
