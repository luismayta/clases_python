# Variables son pasadas por referencia

import copy


def f1(d):
    # Copia de diccionario
    # l = copy.copy(l)
    d = d.copy()
    print 'f', id(d), d
    d['fubar'] = 'fubar'
    print 'f', id(d), d


def f2(l):
    # Copia de listas
    l = l[:]
    print 'f', id(l), l
    l.append('fubar')
    print 'f', id(l), l


def main():
    print "Con diccionario"
    d = {'foo': 'bar'}
    print 'main', id(d), d
    f1(d)
    print 'main', id(d), d

    print "Con lista"
    l = ['foo', 'bar']
    print 'main', id(l), l
    f2(l)
    print 'main', id(l), l


if __name__ == "__main__":
     main()
