# -*- coding: utf-8 -*-


class A(object):

    def get(self):
        print "A"


class B(object):

    def get(self):
        print "B"


class C(A, B):

    def _get(self):
        print "C"


class D(B, A):

    def _get(self):
        print "D"


def main():
    # C().get()
    # D().get()

    print C.mro()
    print D.mro()

if __name__ == '__main__':
    main()
