
class Return(Exception):
    pass


def function1():
    for x in range(10):
        yield x
        if x == 5:
            raise StopIteration()

if __name__ == '__main__':
    print function1
    p = function1()
    # p.next() trae el siguiente valor del generador
    print [i for i in p]
