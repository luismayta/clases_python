from django.conf import settings


def google_analytics(request):
     return {'GA': settings.GOOGLE_ANALYTICS}
