from django import forms

from fbla.models import Snippet


class SnippetForm(forms.ModelForm):

    class Meta:
        model = Snippet
        fields = ('title', 'language', 'text', )
