from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Person(models.Model):

    last_name = models.CharField(max_length=50)
    first_name = models.CharField(max_length=50)

    def __unicode__(self):
        return '{} {}'.format(self.first_name, self.last_name)


class Framework(models.Model):

    name = models.CharField(max_length=50)
    language = models.ForeignKey('Language')

    def __unicode__(self):
        return self.name


class Language(models.Model):

    lexer_name = models.CharField(max_length=20, primary_key=True)
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class Snippet(models.Model):

    title = models.CharField(max_length=50)
    language = models.ForeignKey('Language')
    text = models.TextField()
    html = models.TextField()
    created_ts = models.DateField(auto_now_add=True)

    def __unicode__(self):
        return self.title


@receiver(post_save, sender=Snippet)
def send_email(*args, **kwargs):
    if not kwargs['created']:
        # si el model esta siendo actualizado
        return
    # si el modelo esta siendo insertado
