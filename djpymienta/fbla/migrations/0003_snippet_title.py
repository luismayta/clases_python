# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fbla', '0002_snippet'),
    ]

    operations = [
        migrations.AddField(
            model_name='snippet',
            name='title',
            field=models.CharField(default='Snippet existente', max_length=50),
            preserve_default=False,
        ),
    ]
