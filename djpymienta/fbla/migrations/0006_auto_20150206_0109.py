# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fbla', '0005_language_lexer_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='language',
            name='id',
        ),
        migrations.AlterField(
            model_name='language',
            name='lexer_name',
            field=models.CharField(max_length=20, serialize=False, primary_key=True),
        ),
    ]
