from django.forms.models import model_to_dict
from django.http import Http404
from django.shortcuts import render, redirect
from django.utils.translation import gettext as _
from django.views import generic

from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter
from pygments.util import ClassNotFound

from fbla.forms import SnippetForm
from fbla.models import Snippet


def page_not_found(request):
    return render(request, 'page_not_found.html')


def home(request):
    return render(request, 'home.html')


def create_snippet(request):
    form = SnippetForm()
    if request.method == 'POST':
       form = SnippetForm(request.POST)
       if form.is_valid():
           try:
               lexer = get_lexer_by_name(form.instance.language_id,
                                         stripall=True)
           except ClassNotFound:
               form.instance.html = form.instance.text
           else:
               formatter = HtmlFormatter(linenos=True, cssclass="source")
               form.instance.html = highlight(form.instance.text, lexer,
                                              formatter)
           instance = form.save()
           return redirect('snippet_view', instance.id)

    return render(request, 'create_snippet.html', {'form': form})


class SnippetView(generic.View):

    def get(self, request, _id):
        try:
            snippet = Snippet.objects.get(pk=int(_id))
        except Snippet.DoesNotExist:
            raise Exception(_('Snippet with ID {} does not exists').format(_id))
        else:
            snippet_dict = model_to_dict(snippet)
            snippet_dict['created_ts'] = snippet.created_ts
            snippet_dict['language_name'] = snippet.language.name
            return render(request, 'snippet.html', {'snippet': snippet_dict})


def newone(request):
    keys = ['x', 'y', 'z']
    d = {'x': 12, 'y': 123, 'a': 1234}
    return render(request, 'newone.html', {'keys': keys, 'd': d})
