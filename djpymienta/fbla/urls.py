from django.conf.urls import patterns, url
from fbla.views import SnippetView

urlpatterns = patterns('',
    url(r'^$', 'fbla.views.home', name='home'),
    url(r'^new$', 'fbla.views.create_snippet', name='create_snippet'),
    url(r'^(?P<_id>\d+)$', SnippetView.as_view(), name='snippet_view'),
    url(r'^newone$', 'fbla.views.newone', name='newone'),
)
