from django.contrib import admin
from fbla.models import Framework, Language, Person, Snippet


def update_to_python(modeladmin, request, queryset):
    queryset.update(language_id='python')

update_to_python.short_description = 'Update to Python'


def update_to_php(modeladmin, request, queryset):
    queryset.update(language_id='php')

update_to_php.short_description = 'Update to PHP'


class SnippetAdmin(admin.ModelAdmin):
    list_display = ('title', 'language')
    search_fields = ['title', 'text']
    list_filter = ['language', 'created_ts']
    readonly_fields = ['html', 'created_ts']
    fieldsets = (
        (None, {
            'fields': ['title', 'created_ts']
        }),
        ('Details', {
            # 'classes': ('collapse',),
            'fields': ['language', 'text', 'html']
        }),
    )
    actions = [update_to_python, update_to_php]

admin.site.register(Snippet, SnippetAdmin)
admin.site.register(Person)


class FrameworkInline(admin.TabularInline):

    model = Framework

    def get_extra(self, *args, **kwargs):
        return 0

    def get_max_num(self, request, obj, *args, **kwargs):
        return 10

    def has_add_permission(self, *args, **kwargs):
        return False

    def has_delete_permission(self, *args, **kwargs):
        return False


@admin.register(Language)
class LanguageAdmin(admin.ModelAdmin):

    inlines = [FrameworkInline]
