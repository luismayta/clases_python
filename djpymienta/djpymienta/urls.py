from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'djpymienta.views.home', name='home'),

    url(r'^snippets/', include('fbla.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

handler404 = 'fbla.views.page_not_found'
