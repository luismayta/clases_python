# -*- coding: utf-8 -*-


class IDNoValido(ValueError):

    def __init__(self, _id):
        self.message = '%s no es un ID valido' % _id
        super(IDNoValido, self).__init__(self.message)


def registrar_venta(_id):
    try:
        _id = int(_id)
    except (ValueError, TypeError):
        raise IDNoValido(_id)

def main():
    registrar_venta('skhshdkshd')


if __name__ == '__main__':
    main()
