# Bucles con for


def devolver_primer_par(l):
    for i in l:
        if i % 2 == 0:
            return i
    else:
        return 0


def main():
    primer_par = devolver_primer_par([1, 2])
    print primer_par
    return

    # a = range(10)
    a = []

    for i in a:
        print i
    else:
        print len(a)


def main2():
    cad = 'Cadenaaaaaa'
    for i in cad:
        print i

if __name__ == '__main__':
    main2()
