


class Padre(object):

    def get(self):
        print 'Padre.get'
        self._funcion()
        self.__funcion2()

    def _funcion(self):
        print 'Padre._funcion'

    def __funcion2(self):
        print 'Padre.__funcion2'


class Hija(Padre):

    def get(self):
        print 'Hija.get'
        self._funcion()
        self.__funcion2()


if __name__ == '__main__':
    p = Hija()
    p.get()
