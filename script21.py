

class SlideshowError(Exception):
    pass


class SlideshowValidationError(SlideshowError, TypeError):
    pass


class SlideshowValueError(SlideshowError, ValueError):
    pass


class Slideshow(object):

    def list(self):
        pass

    @classmethod
    def create(self, _id):
        if isinstance(_id, (int, long)):
            raise SlideshowValidationError(_id)
        if _id > 1000:
            raise SlideshowValueError(_id)
        otralibreria.funcion(_id)

    @classmethod
    def delete(self):
        pass


try:
     Slideshow.create('')
except ValueError:
     # 400
except SlideshowError:
     # 500
