# -*- coding: utf-8 -*-


class Padre(object):

    def funcion(self):
        print 'padre'


class Hija(Padre):

    def __funcion(self):
        print 'hija'
        super(Hija, self).funcion()


if __name__ == '__main__':
    p = Padre()
    p.funcion()

    h = Hija()
    h.funcion()
