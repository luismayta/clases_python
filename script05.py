# Condicionales

def main():
    entero = raw_input('Escribir numero: ')
    entero = int(entero)

    if entero % 2 == 0:
        print "%s es par" % entero
    else:
        print "%s es impar" % entero

    mapping = {
        True: 'par',
        False: 'impar'
    }
    r = mapping[entero % 2 == 0]
    print '%s es %s' % (entero, r)


def main2():
    n = raw_input('Escribir numero: ')
    n = int(n)

    if n < 1500:
        print n
    elif n < 180000:
        print n + n * 0.1
    else:
        print n + n * 0.5


def main3():
    d = {'last': 'puente'}

    if 'phone' in d and d['phone'] == '123':
        print d['phone']

    p = d.get('phone', None)
    if p == '123':
        print p


if __name__ == '__main__':
    main3()
