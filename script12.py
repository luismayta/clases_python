# -*- coding: utf-8 -*-


def create_database():
    print 'create database'
    return True


class ORM(object):

    @property
    def database(self):
        if not hasattr(self, '_database'):
            self._database = create_database()
        return self._database

    def nueva_funcion2(self):
        print self

    def nueva_funcion(self, arg1, arg2):
        print self.database, arg1, arg2
        self.nueva_funcion2()
        return False

def main():
    orm = ORM()
    # print orm.database
    # print orm.database

    r = orm.nueva_funcion2()
    print r

if __name__ == '__main__':
    main()
