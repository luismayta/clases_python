# -*- coding: utf-8 -*-
# Manipulación de diccionarios


def main():
    d = {'last': 'puente', 'first': 'jorge'}
    print d, len(d)
    print d.keys()
    print d.values()

    d['phone'] = "23423432423"
    print d

    del d['phone']
    print d

    x = d.get('last', 'P')
    print x

    x = d.get('l', 'PP')
    print x

    for k, v in d.iteritems():
        print k, v

    d.update({'phone': 324324, 'first': 'JJJ'})
    print d

    print 'last' in d

if __name__ == '__main__':
    main()
