# -*- coding: utf-8 -*-


class A:
    pass


class B():
    pass


class Padre(object):

    atributo = 'ATTR'

    @property
    def calculada(self):
        return sum(self.args)

    def __init__(self, *args):
        self.args = args


class Persona(object):

    @property
    def nombre_completo(self):
        return '%s %s' % (self.nombre, self.apellido)

    @nombre_completo.setter
    def nombre_completo(self, value):
        self.nc = value

    def __init__(self, nombre, apellido):
        self.nombre = nombre
        self.apellido = apellido

    def __call__(self):
        print 'CALL'

    def __repr__(self):
        return 'Persona(%s)' % self.nombre_completo

def main():
   p = Padre(1, 2 , 3)
   p.otroatributo = True
   print p.args
   print p.otroatributo
   print p.atributo

   print hasattr(p, 'asd')
   print setattr(p, 'xyz', 'JJ')
   print p.xyz
   print getattr(p, 'args')
   print getattr(p, 'abc', 'asas')


def main2():
    p = Persona('Jorge', 'Puente')
    print p()
    # print p.nombre_completo
    # p.nombre_completo = 'SAKJGSKDJg'
    # print p.nc

if __name__ == '__main__':
    main2()
