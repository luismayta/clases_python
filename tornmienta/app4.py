from tornado.ioloop import IOLoop
from tornado.options import (define, options, parse_command_line,
                             parse_config_file)
from tornado.web import Application, RequestHandler


class HomeHandler(RequestHandler):

    def get(self):
        self.write(options.opcion1)
        self.write(self.application.settings)
        self.write(self.settings)


def opcion3_callback(arg):
    print 'CALLBACK', arg


def main():
    define('opcion1', help='Help de la opcion 1', group='Group 1')
    define('opcion2', default='asd', group='Group 1')
    define('opcion3', callback=opcion3_callback, group='Group 2')
    define('opcion4', metavar='METAVAR', group='Group 3')

    define('debug', type=bool, group='Server')
    define('port', type=int, group='Server')

    parse_config_file('app4.config')
    parse_command_line()
    opciones = options.as_dict()

    app = Application([
                           (r'/', HomeHandler)
                      ], **opciones)

    app.listen(options.port)
    IOLoop.current().start()

if __name__ == '__main__':
    main()
