import functools

from tornado import gen
from tornado.httpclient import AsyncHTTPClient
from tornado.ioloop import IOLoop
from tornado.web import Application, asynchronous, RequestHandler

AsyncHTTPClient.configure("tornado.curl_httpclient.CurlAsyncHTTPClient")
http_client = AsyncHTTPClient()


class HomeHandler(RequestHandler):

    @asynchronous
    def get(self):
        print 'Inicio'
        self.write('Hi!')
        result = http_client.fetch('http://httpbin.org/delay/30',
                                   callback=functools.partial(self.get_result, 'Algo'))
        print result
        #print 'Luego del request'
        #self.finish('Se acabo')

    def get_result(self, variable, response):
        print response, variable
        self.finish('Se acabo')


def main():
    app = Application([(r'/', HomeHandler)], debug=True)
    app.listen(8080)
    IOLoop.current().start()

if __name__ == '__main__':
    print 'Iniciando...'
    main()
