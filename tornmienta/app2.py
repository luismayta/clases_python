from tornado.ioloop import IOLoop
from tornado.web import Application, RequestHandler, url


class HomeHandler(RequestHandler):

    def initialize(self):
        print 'initialize'

    def prepare(self):
        print 'prepare'

    @property
    def db(self):
        return self.settings['db']

    def get(self):
        print 'get'
        print self.application.settings['db']
        print self.settings['db']
        print self.db
        #print self.application.db_connection
        #raise Exception('Este es un error')
        self.render('home.html', v={'d': 123})

    def on_finish(self):
        print 'on_finish'


def main():
    db = 'connection'

    app = Application([
                          url(r'/', HomeHandler, name='home')
                      ],
                      template_path='templates', static_path='static',
                      debug=True, db=db)
    app.listen(8080)
    IOLoop.current().start()


if __name__ == '__main__':
    main()
