from tornado.gen import coroutine
from tornado.httpclient import AsyncHTTPClient
from tornado.ioloop import IOLoop


@coroutine
def do_fetch():
    http_client = AsyncHTTPClient()
    f = yield http_client.fetch('https://google.com')
    print f


def main():
    do_fetch()
    IOLoop.current().start()

if __name__ == '__main__':
    main()
