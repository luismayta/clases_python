import bcrypt

from futures import ThreadPoolExecutor
from tornado import concurrent, gen, ioloop, web


class HomeHandler(web.RequestHandler):

   @property
   def executor(self):
       return self.settings['pool']

   @concurrent.run_on_executor
   def encriptar(self, word):
       e = bcrypt.hashpw(str(word), bcrypt.gensalt(10))
       return e

   @gen.coroutine
   def get(self):
       word = self.get_query_argument('word')
       e = yield self.encriptar(word)

       self.write('Hi ')
       self.write(e)


def main():
    app = web.Application([(r'/', HomeHandler)], debug=True,
                          pool=ThreadPoolExecutor(4))
    app.listen(8080)
    ioloop.IOLoop.current().start()


if __name__ == '__main__':
   main()
