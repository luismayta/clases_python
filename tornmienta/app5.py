import motor

from tornado import gen, ioloop, web


class BaseHandler(web.RequestHandler):

    @property
    def db(self):
        return self.settings['db']


@gen.coroutine
def get_info(db):
    cursor = db.col.find()
    results = yield cursor.to_list(None)
    raise gen.Return(results)


class HomeHandler(BaseHandler):

    @gen.coroutine
    def get(self):
        r = yield get_info(self.db)
        print r
        self.write('Hi')


def main():
    loop = ioloop.IOLoop.current()
    con = motor.MotorClient()
    loop.run_sync(con.open)
    db = con.test

    from functools import partial
    r = loop.run_sync(partial(get_info, db))
    print r

    app = web.Application([(r'/', HomeHandler)], debug=True, db=db)
    app.listen(8080)
    loop.start()

if __name__ == '__main__':
    main()
