import logging

from tornado import ioloop, log, web, websocket

clients = set()


class SocketChatHandler(websocket.WebSocketHandler):

    def check_origin(self, origin):
        return True

    def open(self):
        logging.info('open')
        clients.add(self)

    def on_message(self, message):
        logging.info('on_message: %s' % message)
        for c in clients:
            address = c.request.connection.context.address
            c.write_message('%s: %s' % (address, message))

    def on_close(self):
        logging.info('on_close')
        clients.remove(self)

stream_clients = set()


class SocketStreamHandler(websocket.WebSocketHandler):

    def check_origin(self, origin):
        return True

    def open(self):
        stream_clients.add(self)

    def on_message(self):
        pass

    def on_close(self):
        stream_clients.remove(self)


def send_to_clients():
    logging.info('Send to {}'.format(len(stream_clients)))
    # ...
    for c in stream_clients:
        c.write_message('Hola!')


class HomeHandler(web.RequestHandler):

    def get(self):
        self.write('Hi')


def main():
    log.enable_pretty_logging()

    app = web.Application([
        (r'/', HomeHandler),
        (r'/chat', SocketChatHandler),
        (r'/stream', SocketStreamHandler)
    ], debug=True)
    app.listen(8080)

    periodic_callback = ioloop.PeriodicCallback(send_to_clients, 10000)
    periodic_callback.start()
    ioloop.IOLoop.current().start()

if __name__ == '__main__':
    main()
