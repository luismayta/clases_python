from tornado.ioloop import IOLoop
from tornado.locale import load_translations
from tornado.web import Application, RequestHandler, url


class HomeHandler(RequestHandler):

    def get(self):
        p = self.locale.translate('Hi')
        print p
        self.write({'greetings': p})


def main():
    load_translations('locale')
    app = Application([url(r'/', HomeHandler, name='home')], debug=True)
    app.listen(8080)
    IOLoop.current().start()

if __name__ == '__main__':
    main()
