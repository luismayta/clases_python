# -*- coding: utf-8 -*-


def funcion():
    """Esta es la documentación de la función llamada funcion
    """
    pass


class ClaseA(object):
    """Docs de la Clase A

    Si deseas ....., puedes llamar a :meth:`ClaseA.metodo`.

    :class:`~script23.Return`

    .. code-block:: pycon

       >>> class ClassA(object):
       >>>     pass
    """

    @property
    def propiedad(self):
        """Propiedad de la clase A

        .. warning::

           Esta es una nota
        """
        pass

    def metodo(self):
        """Metodo de la clase A

        .. TODO:

           kkglgaldaldas
        """
        pass


def main():
    print funcion.__doc__
    print ClaseA.__doc__
    print ClaseA().propiedad
    print ClaseA().metodo.__doc__


if __name__ == '__main__':
    main()
