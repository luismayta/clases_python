

class Padre(object):

    def a(self):
        pass

    def b(self):
        pass


class Mixin(object):

    def c(self):
        pass


class Hija(Padre, Mixin):


    def a(self):
        self.c()


if __name__ == '__main__':
    pass
