# -*- coding: utf-8 -*-


class Persona(object):

    def __init__(self):
        self.items = {}

    def __setitem__(self, name, value):
        print name, value
        self.items[name] = value

    def __getitem__(self, name):
        return self.items[name]

    # def __getattr__(self, name):
    #     return self.items[name]

    __getattr__ = __getitem__

def main():
    p = Persona()
    print p.items
    p['nombre'] = 'J'
    print p['nombre']
    print p.nombre

if __name__ == '__main__':
    main()
